import numpy as np
import matplotlib.pyplot as plt
import copy

from . import nets

class PetriTracker:
    def __init__(self, petri_net, record_on_init=False):
        self.net = petri_net
        self.time_vector = np.array([])
        self.markings = np.array([])
        if record_on_init:
            self.record()

    def record(self, marking='auto', time='auto'):
        """Append a record to the tracker."""
        if marking == 'auto':
            marking = copy.copy(self.net.marking)
        if time == 'auto': # set to net time, else time provided
            time = copy.copy(self.net.time)

        if self.markings.shape[0] == 0:
            self.markings = marking.reshape((1, marking.shape[0]))
            self.time_vector = np.array([time])
        else:
            self.markings = np.vstack((self.markings, marking))
            self.time_vector = np.vstack((self.time_vector, np.array([time])))
        return

    def getRecords(self):
        return {'time': self.time_vector, 'marking': self.markings}

    def differentiate(self, degree=1, ignore_t=False):
        """Numerically differentiate markings w.r.t. time."""
        """ATTN: ignores timing right now, ignore_t does nothing"""
        target = self.markings
        target_t = self.time_vector
        diff = np.zeros((target.shape[0] - 1, target.shape[1]))
        diff_t = np.zeros((target_t.shape[0] - 1))
        for _ in range(degree):
            for t in range(target.shape[0] - 1):
                diff[t, :] = target[t+1, :] - target[t, :]
                diff_t[t] = target_t[t+1] - target_t[t]
            target = copy.copy(diff)
            target_t = copy.copy(diff_t)
            diff = np.zeros((target.shape[0] - 1, target.shape[1]))
            diff_t = np.zeros((target_t.shape[0] - 1))
        return target, target_t

    def makeRecordsPlot(self, place_indices, time_min=0, time_max='max', xlabel="Time", ylabel="Tokens", title="Petri net process plot",
                        plot_points=True, scatter_points=False, show=False, place_names=None, differentiation_degree=None, stride=1):
        if differentiation_degree == None:
            data, data_t = self.markings, self.time_vector
        else:
            data_t = self.time_vector[0:-differentiation_degree] + (differentiation_degree / 2) # time plotting vector
            data, _ = self.differentiate(degree=differentiation_degree)
            
        fig, ax = plt.subplots()
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_title(title)
        for i, p in enumerate(place_indices):
            markings = data[:, p]
            if place_names != None:
                place_name = place_names[i]
            if plot_points:
                ax.plot(data_t[::stride], markings[::stride], label=place_name)
            if scatter_points:
                if plot_points:
                    ax.scatter(data_t[::stride], markings[::stride])
                else:
                    ax.scatter(data_t[::stride], markings[::stride], label=place_name)
        ax.legend()
        if show:
            plt.show()
        return fig, ax

    def clear(self):
        self.time_vector = np.array([])
        self.markings = np.array([])
        return

class PetriTrackerAggregator():
    """Aggregate PetriTracker's into one, allows mean, stdev computation."""
    def __init__(self):
        self.time_array = [] # contains means
        self.markings_array = [] # contains means

        self.time_vector_mean = None
        self.markings_mean = None
        self.time_vector_stdev = None
        self.markings_stdev = None
        self.n_vector = None # how many records per time record

    def addTrackerContent(self, tracker, start_index=0):
        """Add a tracker's content to the aggregator. Should pass by reference."""
        self.time_array.append((tracker.time_vector, start_index))
        self.markings_array.append((tracker.markings, start_index))
        return

    def compile(self):
        """Compile the aggregated trackers, calculate means, stdevs. Can throw division by zero if there are not enough samples per time point (at least 2)."""
        if self.time_array == [] or self.markings_array == []:
            raise RuntimeError("@PetriTrackerAggregator.compile: no content to compile!")

        for i, tve in enumerate(self.time_array):
            mke = self.markings_array[i]
            assert tve[1] == mke[1], "@PetriTrackerAggregator.compile: time vector and markings array do not match in start index."
            tv = tve[0]
            mk = mke[0]
            if self.markings_mean != []:
                assert self.markings_mean.shape[1] == mk.shape[1], "@PetriTrackerAggregator.compile: markings place lengths do not match. Are these trackers not tracking the same Petri net?"
            n_places = mk.shape[1]
            assert tv.shape[0] == mk.shape[0], "@PetriTrackerAggregator.compile: time vector and markings array do not match in records length."
            base_length = tve[1] + tv.shape[0]
            relative_length = tv.shape[0]
            current_length = self.n_vector.shape[0]
            if current_length == 0: # initialise
                self.time_vector_mean = np.zeros((base_length))
                self.time_vector_stdev = np.zeros((base_length))
                self.markings_mean = np.zeros((base_length, n_places))
                self.markings_stdev = np.zeros((base_length, n_places))
                self.n_vector = np.zeros((base_length))
                current_length = self.n_vector.shape[0]
            if base_length > current_length: # extension
                np.vstack((self.n_vector, np.zeros((base_length - current_length))))
                np.vstack((self.time_vector_mean, np.zeros((base_length - current_length))))
                np.vstack((self.time_vector_stdev, np.zeros((base_length - current_length))))
                np.vstack((self.markings_mean, np.zeros((base_length - current_length, n_places))))
                np.vstack((self.markings_stdev, np.zeros((base_length - current_length, n_places))))

            self.time_vector_mean[tve[1]:] += tv # addition step
            self.markings_mean[tve[1]:] += mk
            self.n_vector[tve[1]:] += 1

        self.time_vector_mean = np.divide(self.time_vector_mean, self.n_vector) # please don't divide by zero
        self.markings_mean = np.divide(self.markings_mean, self.n_vector)
        self.time_vector_stdev = np.sqrt(np.divide(np.sum(np.array(self.time_array) - self.time_vector_mean, axis=0), self.n_vector - 1)) # again, throws division by zero
        self.markings_stdev = np.sqrt(np.divide(np.sum(np.array(self.markings_array) - self.markings_mean, axis=0), self.n_vector - 1))
        return {'time_mean': self.time_vector_mean, 'time_stdev': self.time_vector_stdev, 'markings_mean': self.markings_mean, 'markings_stdev': self.markings_stdev}

    def differentiateMeans(self, degree=1, ignore_t=False):
        """Numerically differentiate marking means w.r.t. time."""
        """ATTN: ignores timing right now, ignore_t does nothing"""
        if self.markings_mean == None or self.markings_stdev == None:
            raise RuntimeError("@PetriTrackerAggregator.differentiateMeans: aggregator has not succesfully been compiled yet.")

        target = self.markings_mean
        target_t = self.time_vector_mean
        diff = np.zeros((target.shape[0] - 1, target.shape[1]))
        diff_t = np.zeros((target_t.shape[0] - 1))
        for _ in range(degree):
            for t in range(target.shape[0] - 1):
                diff[t, :] = target[t+1, :] - target[t, :]
                diff_t[t] = target_t[t+1] - target_t[t]
            target = copy.copy(diff)
            target_t = copy.copy(diff_t)
            diff = np.zeros((target.shape[0] - 1, target.shape[1]))
            diff_t = np.zeros((target_t.shape[0] - 1))
        return target, target_t

    def makeRecordsPlot(self, place_indices, time_min=0, time_max='max', xlabel="Time", ylabel="Tokens", title="Petri net process plot (aggregate average)",
                        plot_points=True, scatter_points=False, show=False, place_names=None, differentiation_degree=None, stride=1):
        """Plot record means, similar to PetriTracker.makeRecordsPlot."""
        if self.markings_mean == None or self.markings_stdev == None:
            raise RuntimeError("@PetriTrackerAggregator.makeRecordsPlot: aggregator has not succesfully been compiled yet.")
        
        if differentiation_degree == None:
            data, data_t = self.markings_mean, self.time_vector_mean
        else:
            data_t = self.time_vector_mean[0:-differentiation_degree] + (differentiation_degree / 2) # time plotting vector
            data, _ = self.differentiate(degree=differentiation_degree)
            
        fig, ax = plt.subplots()
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_title(title)
        for i, p in enumerate(place_indices):
            markings = data[:, p]
            if place_names != None:
                place_name = place_names[i]
            if plot_points:
                ax.plot(data_t[::stride], markings[::stride], label=place_name)
            if scatter_points:
                if plot_points:
                    ax.scatter(data_t[::stride], markings[::stride])
                else:
                    ax.scatter(data_t[::stride], markings[::stride], label=place_name)
        ax.legend()
        if show:
            plt.show()
        return fig, ax

    def clear(self):
        self.time_array = []
        self.markings_array = []
        self.time_vector_mean = None
        self.markings_mean = None
        self.time_vector_stdev = None
        self.markings_stdev = None
        self.n_vector = None
        return

    def getRecords(self):
        return {'time_mean': self.time_array, 'marking_array': self.markings_array}
