from . import nets

class ParseError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def nestedListNavigate(ls, index):
    nls = ls
    for ind in index:
        nls = nls[ind]
    return nls

def nestedListNestNew(ls, index, value):
    sub = nestedListNavigate(ls, index)
    sub.append(value)
    depth = len(index)
    for d in reversed(range(depth)):
        nls = nestedListNavigate(ls, index[0:d])
        nls[index[d]] = sub
        sub = nls
    return

def nestedListSet(ls, index, value):
    sub = nestedListNavigate(ls, index[0:-1])
    sub[index[-1]] = value
    depth = len(index) - 1
    for d in reversed(range(depth)):
        nls = nestedListNavigate(ls, index[0:d])
        nls[index[d]] = sub
        sub = nls
    return

def recursivePrint(ls):
    if type(ls) == list or type(ls) == tuple:
        for elem in ls:
            recursivePrint(elem)
    else:
        print(ls)
    return

def parseValueString(string):
    bracket_scope = 1
    nested_index_list = [0]
    element = [None]
    current_string = ""
    for c in string:
        if c == "[" or c == "(":
            nestedListSet(element, nested_index_list[0:bracket_scope], [None])
            bracket_scope += 1
            nested_index_list.append(0)
        elif c == "]" or c == ")":
            if current_string != "":
                nestedListSet(element, nested_index_list[0:bracket_scope], current_string)
                current_string = ""
            bracket_scope -= 1
            if bracket_scope < 1:
                raise ParseError("@parseValueString: list scope decreased below zero. Misplaced brackets?")
        elif c == ",":
            if current_string != "":
                nestedListSet(element, nested_index_list[0:bracket_scope], current_string)
                current_string = ""
            nestedListNestNew(element, nested_index_list[0:bracket_scope-1], None)
            nested_index_list[bracket_scope - 1] += 1
            if len(nested_index_list) > bracket_scope:
                for d in range(bracket_scope, len(nested_index_list)):
                    nested_index_list[d] = 0
        else:
            current_string += c
    if current_string != "":
        nestedListSet(element, nested_index_list[0:bracket_scope], current_string)
        current_string = ""
            
    if bracket_scope != 1:
        print("@parseValueString: WARNING: list scope not zero at end of parse. Misplaced brackets?")
    return element[0]

def readNetFileSimplePetriNet(filename):
    file = open(filename).read()
    global_scope_open = False
    kv_read_mode = "key"
    value_list_scope = 0
    fields_remaining = {"version": True,
                        "net_type": True,
                        "places": True,
                        "transitions": True,
                        "arcs": True}
    fields_kv = {}
    current_element = ""
    current_key = ""
    for c in file:
        if c == "{":
            if global_scope_open == False:
                global_scope_open = True
            else:
                raise ParseError("@readNetFileSimplePetriNet: invalid global scoping")
        elif c == "}":
            if value_list_scope == 0:
                parsed = parseValueString(current_element)
                fields_kv[current_key] = parsed
                current_element = ""
                current_key = ""
                kv_read_mode = "key"
            else:
                raise ParseError("@readNetFileSimplePetriNet: list scope not zero at end. Misplaced brackets?")
                
            if global_scope_open == True:
                global_scope_open = False
                break
            else:
                raise ParseError("@readNetFileSimplePetriNet: invalid global scoping")
        else:
            if kv_read_mode == "key":
                if c == "=":
                    kv_read_mode = "value"
                    if current_element not in fields_remaining.keys():
                        raise ParseError("@readNetFileSimplePetriNet: key not found: " + current_element)
                    else:
                        fields_remaining[current_element] = False
                        fields_kv[current_element] = None
                        current_key = current_element
                        current_element = ""
                else:
                    if c != "\n" and c != " ": # ignore newline and whitespace
                        current_element += c
            elif kv_read_mode == "value":
                if c == "[" or c == "(":
                    current_element += c
                    value_list_scope += 1
                elif c == "]" or c == ")":
                    current_element += c
                    value_list_scope -= 1
                    if value_list_scope < 0:
                        raise ParseError("@readNetFileSimplePetriNet: list scope decreased below zero. Misplaced brackets?")
                elif c == ",":
                    if value_list_scope == 0:
                        parsed = parseValueString(current_element)
                        fields_kv[current_key] = parsed
                        current_element = ""
                        current_key = ""
                        kv_read_mode = "key"
                    else:
                        current_element += c
                else:
                    if c != "\n" and c != " ": # ignore newline and whitespace
                        current_element += c
    for k, v in fields_remaining.items():
        if v == True:
            print("@readNetFileSimplePetriNet: WARNING: missing key:" + k)
    return fields_kv
            
