import numpy as np
import copy
import random

from . import nets

def normalise(vector, norm_value):
    """Normalise a vector to a sum of norm_value. Should work for arrays too."""
    return (norm_value * (vector / np.sum(vector)))

class PetriProcess:
    def __init__(self, petri_net):
        self.petri_net = petri_net

    def fire(self):
        """Default firing: change nothing."""
        to_fire = []
        new_marking = self.petri_net.marking
        liveness = [net.checkSingleLivenessWithMarking(tr, new_marking) for tr in net.allTransitionIndices()]
        return to_fire, new_marking, liveness

    def fireContinuously(self, max_cycles=None, force_max_cycles=False, callback=None, **kwargs):
        """Fire the process continuously until no more transitions can be fired."""
        count = 0
        max_cycles_numeric = [max_cycles, -1][max_cycles == None]
        if 'return_liveness' in kwargs.keys():
            del kwargs['return_liveness'] # this argument will be overwritten
        while True:
            to_fire, new_marking, liveness = self.fire(**kwargs, return_liveness=True)
            if callback is not None:
                signal_stop = callback(to_fire, new_marking, count)
            count += 1
            if (count >= max_cycles_numeric and max_cycles is not None) or signal_stop: # not allowed to fire
                break
            if sum(liveness) <= 0: # cannot fire any transitions anymore
                if not force_max_cycles: # else we just continue 'firing' with nothing to fire, useful for e.g. trackers
                    break
        return to_fire, new_marking, count

class PetriProcessSimpleStochastic(PetriProcess):
    def __init__(self, petri_net, alpha):
        if type(petri_net == nets.SimplePetriNet):
            self.petri_net = petri_net
        else:
            raise TypeError("@PetriProcessSimpleStochastic.__init__: Petri net type not supported")
        self.alpha = alpha # stopping probability per firing attempt         

    def fire(self, update_marking=True, update_time=True, force_fire_first_round=False, calc_mode="tmatrix", return_liveness=False):
        """Fire the process once. May fire multiple transitions, alpha controls firing/stopping probability."""
        net = self.petri_net
        remaining_marking = copy.deepcopy(self.petri_net.marking)
        n_firings = 0
        stop = False
        to_fire = []
        while not stop:
            transitions = net.allTransitionIndices()
            liveness = [net.checkSingleLivenessWithMarking(tr, remaining_marking) for tr in transitions]
            n_live_transitions = sum(liveness) # boolean sum?
            if n_live_transitions <= 0: # no more transitions to fire
                break
            
            if random.random() > self.alpha or (n_firings == 0 and force_fire_first_round == True):
                n_firings += 1
                firing_probability = 1 / n_live_transitions
                random_choice = random.random()
                cumul = 0
                choice = None
                for i in range(len(transitions)):
                    if liveness[i]:
                        cumul += firing_probability
                        if cumul >= random_choice:
                            choice = i
                            break
                if choice is None:
                    raise RuntimeError("@PetriProcessSimpleStochastic.fire: failed to choose transition to fire, exceeded cumulative bound.")
                to_fire.append(choice)
                for mark in self.petri_net.transition_dict[choice]["in"]:
                    remaining_marking[mark[0]] -= mark[1] # subtract remaining
            else:
                break

        new_marking = net.fireTransitions(to_fire, skip_liveness_check=True, calc_mode=calc_mode, update_marking=update_marking, update_time=update_time)
        if not return_liveness:
            return to_fire, new_marking
        else:
            liveness = [net.checkSingleLivenessWithMarking(tr, new_marking) for tr in transitions] # new liveness
            return to_fire, new_marking, liveness
            
class PetriProcessSimpleStochasticAlphaDebiased(PetriProcess):
    def __init__(self, petri_net, alpha):
        if type(petri_net == nets.SimplePetriNet):
            self.petri_net = petri_net
        else:
            raise TypeError("@PetriProcessSimpleStochasticAlphaDebiased.__init__: Petri net type not supported")
        self.alpha = alpha # stopping probability per firing attempt, debiased towards higher liveness         

    def fire(self, update_marking=True, update_time=True, force_fire_first_round=False, calc_mode="tmatrix", return_liveness=False):
        """Fire the process once. May fire multiple transitions, alpha controls firing/stopping probability."""
        net = self.petri_net
        remaining_marking = copy.deepcopy(self.petri_net.marking)
        n_firings = 0
        stop = False
        to_fire = []
        while not stop:
            transitions = net.allTransitionIndices()
            liveness = [net.checkSingleLivenessWithMarking(tr, remaining_marking) for tr in transitions]
            n_live_transitions = sum(liveness) # boolean sum?
            if n_live_transitions <= 0: # no more transitions to fire
                break
            true_alpha = self.alpha * (2 / (n_live_transitions + 1)) # alpha unbiasing
            
            if random.random() > true_alpha or (n_firings == 0 and force_fire_first_round == True):
                n_firings += 1
                firing_probability = 1 / n_live_transitions
                random_choice = random.random()
                cumul = 0
                choice = None
                for i in range(len(transitions)):
                    if liveness[i]:
                        cumul += firing_probability
                        if cumul >= random_choice:
                            choice = i
                            break
                if choice is None:
                    raise RuntimeError("@PetriProcessSimpleStochasticAlphaDebiased.fire: failed to choose transition to fire, exceeded cumulative bound.")
                to_fire.append(choice)
                for mark in self.petri_net.transition_dict[choice]["in"]:
                    remaining_marking[mark[0]] -= mark[1] # subtract remaining
            else:
                break

        new_marking = net.fireTransitions(to_fire, skip_liveness_check=True, calc_mode=calc_mode, update_marking=update_marking, update_time=update_time)
        if not return_liveness:
            return to_fire, new_marking
        else:
            liveness = [net.checkSingleLivenessWithMarking(tr, new_marking) for tr in transitions] # new liveness
            return to_fire, new_marking, liveness        
                    
class PetriProcessSimpleStochasticAlphaDebiasedRated(PetriProcess):
    def __init__(self, petri_net, alpha, firing_rates):
        if type(petri_net == nets.SimplePetriNet):
            self.petri_net = petri_net
        else:
            raise TypeError("@PetriProcessSimpleStochasticAlphaDebiasedRated.__init__: Petri net type not supported")
        self.alpha = alpha # stopping probability per firing attempt, debiased towards higher liveness
        if len(firing_rates) != len(petri_net.transitions):
            raise IndexError("@PetriProcessSimpleStochasticAlphaDebiasedRated: firing rate list does not match number of transitions")
        for fr in firing_rates:
            if fr < 0:
                raise ValueError("@PetriProcessSimpleStochasticAlphaDebiasedRated: firing rate list contains negative value(s)")
        self.firing_rates = np.array(firing_rates)

    def fire(self, update_marking=True, update_time=True, force_fire_first_round=False, calc_mode="tmatrix", return_liveness=False):
        """Fire the process once. Controllable firing rates. May fire multiple transitions, alpha controls firing/stopping probability."""
        net = self.petri_net # reference
        remaining_marking = copy.deepcopy(self.petri_net.marking)
        n_firings = 0
        stop = False
        to_fire = []
        while not stop:
            transitions = net.allTransitionIndices()
            liveness = [net.checkSingleLivenessWithMarking(tr, remaining_marking) for tr in transitions]
            live_transitions = [transitions[i] for i in range(len(liveness)) if liveness[i] == True]
            n_live_transitions = sum(liveness)
            firing_sum = sum([fr for i, fr in enumerate(self.firing_rates) if liveness[i] == True]) # firing rate sum
            if firing_sum <= 0: # no more transitions to fire
                break
            true_alpha = self.alpha * (2 / (firing_sum + 1)) # alpha unbiasing, using the absolute firing rate sum here!
            norm_firing_rates = normalise(self.firing_rates[live_transitions], 1) # normalise to 1
            if random.random() > true_alpha or (n_firings == 0 and force_fire_first_round == True):
                n_firings += 1
                random_choice = random.random()
                cumul = 0
                choice = None
                for i, tr in enumerate(live_transitions):
                    cumul += norm_firing_rates[i]
                    if cumul >= random_choice:
                        choice = tr
                        #print(net.getTransitionLabel(choice))
                        break
                if choice is None:
                    raise RuntimeError("@PetriProcessSimpleStochasticAlphaDebiased.fire: failed to choose transition to fire, exceeded cumulative bound.")
                to_fire.append(choice)
                for mark in self.petri_net.transition_dict[choice]["in"]:
                    remaining_marking[mark[0]] -= mark[1] # subtract remaining
            else:
                break

        new_marking = net.fireTransitions(to_fire, skip_liveness_check=True, calc_mode=calc_mode, update_marking=update_marking, update_time=update_time)
        if not return_liveness:
            return to_fire, new_marking
        else:
            liveness = [net.checkSingleLivenessWithMarking(tr, new_marking) for tr in transitions] # new liveness
            return to_fire, new_marking, liveness        

def lambdaDistribution(t, lamb):
    """Return the lambda distribution."""
    """Probability that a lambda process fires within t."""
    return (1 - np.exp((-1 * t * lamb)))

def lambdaDistributionTime(p, lamb):
    """Return t associated with p with lambda distribution."""
    if type(p) != np.ndarray:
        if p < 0 or p > 1:
            raise ValueError("@lambdaDistributionTime: p must be between 0 and 1""")
    else:
        for pv in p:
            if pv < 0 or pv > 1:
                raise ValueError("@lambdaDistributionTime: p must be between 0 and 1""")

    return (np.log(1 - p) / lamb) # base=math.e

def multiAntiLambdaDistribution(t, lambda_vector):
    """Lambda distribution with multiple lambda processes simultaneously."""
    """Probability that a sequence of lambda processes does not fire within t."""
    product = np.prod(lambda_vector)
    n = lambda_vector.shape[0]
    return (np.exp(-1 * np.power(t, n) * product))

def multiAntiLambdaDistributionTime(p, lambda_vector, inf_value=float('inf')):
    """Return t for an anti-lambda p parametrized by lambda_vector lambda processes."""
    n = lambda_vector.shape[0]
    product = np.prod(lambda_vector)
    if p <= 0 or n <= 0 or product <= 0: # probability zero, or no events to fire
        return inf_value
    return np.power(((-1 * np.log(p)) / product), (1 / n))

class PetriProcessSimpleCTPNRated(PetriProcess):
    def __init__(self, petri_net, firing_rates):
        if type(petri_net == nets.SimplePetriNet):
            self.petri_net = petri_net
        else:
            raise TypeError("@PetriProcessSimpleCTPNRated.__init__: Petri net type not supported")

        if len(firing_rates) != len(petri_net.transitions):
            raise IndexError("@PetriProcessSimpleCTPNRated: firing rate list does not match number of transitions")
        for fr in firing_rates:
            if fr < 0:
                raise ValueError("@PetriProcessSimpleCTPNRated: firing rate list contains negative value(s)")
        self.firing_rates = np.array(firing_rates)

    def fire(self, update_marking=True, update_time=True, calc_mode="tmatrix", return_liveness=False):
        """Fire the process once. Controllable firing rates. May fire multiple transitions, alpha controls firing/stopping probability.
            Uses time-simulated firing with a continuous-time Markov-like model."""
        net = self.petri_net # reference
        to_fire = []
        skip = False
        
        transitions = net.allTransitionIndices()
        liveness = [net.checkSingleLivenessWithMarking(tr, net.marking) for tr in transitions]
        live_transitions = [transitions[i] for i in range(len(liveness)) if liveness[i] == True]
        lambda_vector = self.firing_rates[live_transitions]
        n_live_transitions = sum(liveness)
        if n_live_transitions <= 0 or np.sum(lambda_vector) <= 0: # no more transitions to fire
            skip = True

        if not skip:
            norm_lambda_vector = normalise(lambda_vector, 1)
            random_wait_choice = random.random()
            random_wait_time = multiAntiLambdaDistributionTime(random_wait_choice, lambda_vector) # wait for a random amount of time
            random_transition_choice = random.random()
            cumul = 0
            choice = None
            for i, tr in enumerate(live_transitions):
                cumul += norm_lambda_vector[i]
                if cumul >= random_transition_choice:
                    choice = tr
                    break
            if choice is None:
                raise RuntimeError("@PetriProcessSimpleCTPNRated.fire: failed to choose transition to fire, exceeded cumulative bound.")
            to_fire.append(choice)
        else:
            random_wait_time = float('inf') # infinity waiting time

        if update_time and not skip:
            net.time += random_wait_time # update random waiting time
        new_marking = net.fireTransitions(to_fire, skip_liveness_check=True, calc_mode=calc_mode, update_marking=update_marking, update_time=False)
        if not return_liveness:
            if update_time:
                return to_fire, new_marking
            else:
                return to_fire, new_marking, random_wait_time      
        else:
            if update_time:
                liveness = [net.checkSingleLivenessWithMarking(tr, new_marking) for tr in transitions] # new liveness
                return to_fire, new_marking, liveness
            else:
                return to_fire, new_marking, liveness, random_wait_time
