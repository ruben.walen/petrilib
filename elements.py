from . import nets

"""A collection of common Petri net elements."""

def activatorElement(net, label_prefix, input_label=None, input_type=None, input_weight=1,
                     output_label=None, output_type=None, output_weight=1, suffixes=(("_av"), ("_a"))):
    """Add an activator element to the network. Shape varies with input/output type.
    Input: place: connects to place; transition: connects to transition, creates new activator place; none: creates new activator place.
    Ouput: place: connects to place (activating); transition: connects to transition; none: transition is terminal."""
    if input_type not is None and input_label is None:
        raise ValueError("@elements.activatorElement: when using input/output transitions or places, labels must be provided.")
    if output_type not is None and output_label is None:
        raise ValueError("@elements.activatorElement: when using input/output transitions or places, labels must be provided.")
    types = ["place", "transition", None]
    if input_type not in types or output_type not in types:
        raise ValueError("@elements.activatorElement: invalid input/output type.")

    # core
    if input_type is not "place":
        core_place_name = label_prefix + suffixes[0][0]
        net.addPlace(core_name) # core place
    else:
        core_place_name = input_label
    if input_type is "transition":
        net.addArc("out", net.getPlaceIndex(core_place_name), net.getTransitionIndex(input_label), input_weight)

    if output_type is not "transition":
        net.addTransition(label_prefix + suffixes[1][0]) # core transition
        core_transition_name = label_prefix + suffixes[1][0]
    else:
        core_transition_name = output_label
        
    net.addArc("in", net.getPlaceIndex(core_place_name), net.getTransitionIndex(core_transition_name), output_weight) # core input arc
    if output_type is "place":
        net.addArc("out", net.getPlaceIndex(output_label), net.getTransitionIndex(core_transition_name), output_weight) # additional output arc

def deactivatorElement(net, label_prefix, input_label=None, input_type=None, input_weight=1, core_weight=1
                     output_label=None, output_type=None, output_weight=1, suffixes=(("_dv", "_d"), ("_d"))):
    """Add a deactivator element to the network. Shape varies with input/output type.
    Input: place: connects to place; transition: connects to transition, creates new deactivator place; none: creates new deactivator place.
    Ouput: place: connects to place (deactivating); transition: INVALID OUPUT; none: connects to new ouput place (deactivating)."""
    if input_type not is None and input_label is None:
        raise ValueError("@elements.deactivatorElement: when using input/output transitions or places, labels must be provided.")
    if output_type not is None and output_label is None:
        raise ValueError("@elements.deactivatorElement: when using input/output transitions or places, labels must be provided.")
    types = ["place", "transition", None]
    if input_type not in types or output_type not in types:
        raise ValueError("@elements.deactivatorElement: invalid input/output type.")
    if output_type is "transition":
        raise ValueError("@elements.deactivatorElement: transition output type is not sensible for deactivator element.")

    # core
    if input_type is not "place":
        core_place_name = label_prefix + suffixes[0][0]
        net.addPlace(core_name) # core place
    else:
        core_place_name = input_label
    if input_type is "transition":
        net.addArc("out", net.getPlaceIndex(core_place_name), net.getTransitionIndex(input_label), input_weight)

    net.addTransition(label_prefix + suffixes[1][0]) # core transition
    core_transition_name = label_prefix + suffixes[1][0]

    if output_type is None:
        net.addPlace(label_prefix + suffixes[0][1]) # deactivated place
        output_place = label_prefix + suffixes[0][1]
    else:
        output_place = output_label
        
    net.addArc("in", net.getPlaceIndex(core_place_name), net.getTransitionIndex(core_transition_name), core_weight) # core input arc
    net.addArc("in", net.getPlaceIndex(output_place), net.getTransitionIndex(core_transition_name), output_weight) # deactivator arc

def catalyticElement(net, act_deact='activate', label_prefix, input_label=None, input_type=None, input_weight=1, core_weight=1
                     output_label=None, output_type=None, output_weight=1, suffixes=(("_cat", "_out"), ("_c"))):
    """Add a catalytic connection element to the network. Shape varies with input/output type.
    Input: place: connects catalytically to place; transition: connects to transition, creates new catalytic place; none: creates new catalytic place.
    Ouput: place: connects to place (a/d); transition: connects to transition (a/d arc); none: connects to new ouput place (a/d).
    act_deact ("activate" or "deactivate"): direction of ouput non-catalytics arc. Not used when output_type='transition' (always "in" arc)"""
    if input_type not is None and input_label is None:
        raise ValueError("@elements.catalyticElement: when using input/output transitions or places, labels must be provided.")
    if output_type not is None and output_label is None:
        raise ValueError("@elements.catalyticElement: when using input/output transitions or places, labels must be provided.")
    types = ["place", "transition", None]
    if input_type not in types or output_type not in types:
        raise ValueError("@elements.catalyticElement: invalid input/output type.")
    if act_deact not in ['activate', 'deactivate']:
        raise ValueError("@elements.catalyticElement: act_deact must be 'activate' or 'deactivate'.")

    # core
    if input_type is not "place":
        core_place_name = label_prefix + suffixes[0][0]
        net.addPlace(core_name) # core place
    else:
        core_place_name = input_label
    if input_type is "transition":
        net.addArc("out", net.getPlaceIndex(core_place_name), net.getTransitionIndex(input_label), input_weight)
    
    net.addTransition(label_prefix + suffixes[1][0]) # core transition
    core_transition_name = label_prefix + suffixes[1][0]
    net.addArc("in", net.getPlaceIndex(core_place_name), net.getTransitionIndex(core_transition_name), core_weight)
    net.addArc("out", net.getPlaceIndex(core_place_name), net.getTransitionIndex(core_transition_name), core_weight)

    in_out = ["in", "out"][act_deact == 'activate']

    if output_type is None:
        net.addPlace(label_prefix + suffixes[0][1]) # catalysed place
        output_place = label_prefix + suffixes[0][1]
        net.addArc(in_out, net.getPlaceIndex(core_place_name), net.getTransitionIndex(core_transition_name), output_weight)
    elif output_type is "place":
        output_place = output_label
        net.addArc(in_out, net.getPlaceIndex(output_label), net.getTransitionIndex(core_transition_name), output_weight)
    elif output_type is "transition":
        net.addArc("in", net.getPlaceIndex(output_label), net.getTransitionIndex(core_transition_name), output_weight) # in_out does not make sense here
        
def enzymaticElement(net, label_prefix, activator_input_label=None, activator_input_type=None, activator_input_weight=1,
                     deactivator_input_label=None, deactivator_input_type=None, deactivator_input_weight=1,
                     activation_weight=1, deactivation_weight=1,
                     output_label=None, output_type=None, output_weight=1,
                     suffixes=(("_a", "_d"), ("_a", "_d"))):
    """Add a catalytic connection element to the network. Shape varies with input/output type.
    Input activator: place: connects place to activator transition; none: activator transition only connects _a and _d.
    Input deactivator: place: connects place to deactivator transition; none: deactivator transition only connects _a and _d.
    Ouput: transition: connects to transition ("in" arc); none: does not connect.
    Core kinetic parameters: activation_weight, deactivation_weight. Markings _a + _d is a constant in this closed system."""
    if activator_input_type not is None and activator_input_label is None:
        raise ValueError("@elements.enzymaticElement: when using input/output transitions or places, labels must be provided.")
    if deactivator_input_type not is None and deactivator_input_label is None:
        raise ValueError("@elements.enzymaticElement: when using input/output transitions or places, labels must be provided.")
    if output_type not is None and output_label is None:
        raise ValueError("@elements.enzymaticElementt: when using input/output transitions or places, labels must be provided.")
    if activator_input_type not in types or deactivator_input_type not in ["place", None] or output_type not in ["transition", None]:
        raise ValueError("@elements.enzymaticElement: invalid input/output type.")

    # core
    net.addTransition(label_prefix + suffixes[1][0]) # activator
    net.addTransition(label_prefix + suffixes[1][1]) # deactivator
    net.addPlace(label_prefix + suffixes[0][0]) # active form
    net.addPlace(label_prefix + suffixes[0][1]) # inactive form

    net.addArc("in", net.getPlaceIndex(label_prefix + suffixes[0][1]), net.getTransitionIndex(label_prefix + suffixes[1][0]), activation_weight) # act
    net.addArc("out", net.getPlaceIndex(label_prefix + suffixes[0][0]), net.getTransitionIndex(label_prefix + suffixes[1][0]), activation_weight) # act
    net.addArc("in", net.getPlaceIndex(label_prefix + suffixes[0][0]), net.getTransitionIndex(label_prefix + suffixes[1][1]), deactivation_weight) # deact
    net.addArc("out", net.getPlaceIndex(label_prefix + suffixes[0][1]), net.getTransitionIndex(label_prefix + suffixes[1][1]), deactivation_weight) # deact

    if activator_input_type is "place":
        net.addArc("in", net.getPlaceIndex(activator_input_label), net.getTransitionIndex(label_prefix + suffixes[1][0]), activator_input_weight)
    if deactivator_input_type is "place":
        net.addArc("in", net.getPlaceIndex(deactivator_input_label), net.getTransitionIndex(label_prefix + suffixes[1][1]), deactivator_input_weight)
    if output_type is "transition":
        net.addArc("in", net.getPlaceIndex(label_prefix + suffixes[0][0]), net.getTransitionIndex(output_label), output_weight) # from active form

