#from petrilib import nets, process, tracker
import petrilib.nets

desens = nets.SimplePetriNet()
desens.constructFromFile("./desensitisation.txt")

# Marking
marking = [0 for _ in range(len(desens.places))]
marking[desens.getPlaceIndex('NFAT_P')] = 200
marking[desens.getPlaceIndex('GSK3b')] = 40
marking[desens.getPlaceIndex('adrenalin')] = 250
marking[desens.getPlaceIndex('desensitisation')] = 100
desens.setMarking(marking)

# Rates
rates = [1 for _ in range(len(desens.transitions))]
rates[desens.getTransitionIndex("adrenalin_release")] = 2
rates[desens.getTransitionIndex("desensitisation")] = 2

pnp = process.PetriProcessSimpleCTPNRated(desens, rates)
pnt = tracker.PetriTracker(desens, record_on_init=True)

max_time = 10000
def process_callback(to_fire, new_marking, count):
    #print("Count", count)
    pnt.record()
    if max_time is not None:
        if desens.time > max_time:
            return True # stop
    return False # stop?

pnp.fireContinuously(callback=process_callback, max_cycles=None, calc_mode='tmatrix')

to_plot_names = ["hypertrophic_TFs", "GSK3b", "Akt", "Src", "adrenalin", "desensitisation"]
to_plot_indices = [desens.getPlaceIndex(name) for name in to_plot_names]
pnt.makeRecordsPlot(to_plot_indices, show=True, place_names=to_plot_names, differentiation_degree=None, plot_points=True, scatter_points=False, stride=1)
