import process
import nets
import tracker

import numpy as np

inf_net = nets.SimplePetriNet()

inf_net.addPlace("S")
inf_net.addPlace("I")
inf_net.addPlace("R")
inf_net.addPlace("D")

inf_net.addTransition("infect")
inf_net.addTransition("die")
inf_net.addTransition("recover")

inf_net.addArc("in", inf_net.getPlaceIndex("S"), inf_net.getTransitionIndex("infect"), 1)
inf_net.addArc("out", inf_net.getPlaceIndex("I"), inf_net.getTransitionIndex("infect"), 1)
inf_net.addArc("in", inf_net.getPlaceIndex("I"), inf_net.getTransitionIndex("die"), 1)
inf_net.addArc("out", inf_net.getPlaceIndex("D"), inf_net.getTransitionIndex("die"), 1)
inf_net.addArc("in", inf_net.getPlaceIndex("I"), inf_net.getTransitionIndex("recover"), 1)
inf_net.addArc("out", inf_net.getPlaceIndex("R"), inf_net.getTransitionIndex("recover"), 1)

def calcRates(marking, scaler=0.2, rd_speed=0.5, infectiousness=1, death_rate_constant=0.03, healthcare_capacity=500, death_rate_no_healthcare=0.10):
    # Calculate transition rates from a given marking
    infections = marking[1]
    susceptible = marking[0]

    if infections > healthcare_capacity and infections > 0:
        death_rate_constant = ((healthcare_capacity * death_rate_constant) + ((infections - healthcare_capacity) * death_rate_no_healthcare)) / infections

    infection_rate = scaler * infections * infectiousness
    recovery_rate = scaler * rd_speed * (1 - death_rate_constant) * infections
    death_rate = scaler * rd_speed * death_rate_constant * infections
    
    rates = [infection_rate, death_rate, recovery_rate]
    return rates

base_infectiousness = 1
lockdown_infectiousness = 0.4
lockdown_time = 280
S_pool = 5000
I_start = 10

markings = [0 for pl in inf_net.allPlaceIndices()]
markings[0] = S_pool
markings[1] = I_start

rates = calcRates(markings)
inf_net.setMarking(markings)
pnp = process.PetriProcessSimpleCTPNRated(inf_net, rates)
pnt = tracker.PetriTracker(inf_net, record_on_init=True)

def process_callback(to_fire, new_marking, count):
    if count % 1000 == 0:
        print("Count", count)
        
    if inf_net.time < lockdown_time:
        new_infectiousness = base_infectiousness
    else:
        new_infectiousness = lockdown_infectiousness
        
    pnp.firing_rates = np.array(calcRates(new_marking, infectiousness=new_infectiousness))
    pnt.record()
    stop = [False, True][bool(np.sum(pnp.firing_rates) <= 0)]
    return stop

pnp.fireContinuously(callback=process_callback, max_cycles=None, force_max_cycles=False, calc_mode='tmatrix')
print("Never infected:", inf_net.marking[0], "Infected (end):", inf_net.marking[1], "Recovered:", inf_net.marking[2], "Dead:", inf_net.marking[3])
pnt.makeRecordsPlot(inf_net.allPlaceIndices(), show=True, place_names=inf_net.allPlaceLabels(), differentiation_degree=None, plot_points=True, scatter_points=False, stride=10)

#################
import matplotlib.pyplot as plt

lockdown_times = [x for x in range(100, 500, 10)]
repeat = 15

ip_vector = []
cfr_vector = []
td_vector = []

for lt in lockdown_times:
    ip_mean = 0
    cfr_mean = 0
    td_mean = 0
    for r in range(repeat):
        print("Lockdown t = ", lt, " Repeat = ", r + 1)
        inf_net.reset()
        lockdown_time = lt
        markings = [0 for pl in inf_net.allPlaceIndices()]
        markings[0] = S_pool
        markings[1] = I_start
        rates = calcRates(markings)
        pnp.firing_rates = np.array(rates)
        inf_net.setMarking(markings)

        pnt.clear()
        pnp.fireContinuously(callback=process_callback, max_cycles=None, force_max_cycles=False, calc_mode='tmatrix')

        infected_percent = (inf_net.marking[1] + inf_net.marking[2] + inf_net.marking[3]) / (S_pool + I_start)
        cfr_final = (inf_net.marking[3]) / (inf_net.marking[1] + inf_net.marking[2] + inf_net.marking[3])
        total_deaths = inf_net.marking[3]

        ip_mean += infected_percent
        cfr_mean += cfr_final
        td_mean += total_deaths

    ip_vector.append(ip_mean / repeat) # divide here
    cfr_vector.append(cfr_mean / repeat)
    td_vector.append(td_mean / repeat)

fig, ax = plt.subplots()
ax.plot(lockdown_times, ip_vector)
ax.set_title("Infection ratio vs. lockdown time")
ax.set_xlabel("Lockdown time")
ax.set_ylabel("Infection ratio")
plt.show()

fig, ax = plt.subplots()
ax.plot(lockdown_times, cfr_vector)
ax.set_title("CFR vs. lockdown time")
ax.set_xlabel("Lockdown time")
ax.set_ylabel("CFR")
plt.show()

fig, ax = plt.subplots()
ax.plot(lockdown_times, td_vector)
ax.set_title("Total deaths vs. lockdown time")
ax.set_xlabel("Lockdown time")
ax.set_ylabel("Total deaths")
plt.show()
