import nets
import tracker
import process

pn2 = nets.SimplePetriNet()

pn2.addPlace("p0")
pn2.addPlace("p1d")
pn2.addPlace("p1a")
pn2.addPlace("p2d")
pn2.addPlace("p2a")

pn2.addTransition("t1a")
pn2.addTransition("t1d")
pn2.addTransition("t2a")
pn2.addTransition("t2d")
pn2.addTransition("t0leak")

coeff1 = 5
coeff2 = 2
pn2.addArc("in", pn2.getPlaceIndex("p0"), pn2.getTransitionIndex("t1a"), 1)
pn2.addArc("out", pn2.getPlaceIndex("p0"), pn2.getTransitionIndex("t1a"), 1)
pn2.addArc("in", pn2.getPlaceIndex("p1d"), pn2.getTransitionIndex("t1a"), coeff1)
pn2.addArc("out", pn2.getPlaceIndex("p1a"), pn2.getTransitionIndex("t1a"), coeff1)

pn2.addArc("in", pn2.getPlaceIndex("p1a"), pn2.getTransitionIndex("t2a"), 1)
pn2.addArc("out", pn2.getPlaceIndex("p1a"), pn2.getTransitionIndex("t2a"), 1)
pn2.addArc("in", pn2.getPlaceIndex("p2d"), pn2.getTransitionIndex("t2a"), coeff2)
pn2.addArc("out", pn2.getPlaceIndex("p2a"), pn2.getTransitionIndex("t2a"), coeff2)

pn2.addArc("in", pn2.getPlaceIndex("p1a"), pn2.getTransitionIndex("t1d"), 1)
pn2.addArc("out", pn2.getPlaceIndex("p1d"), pn2.getTransitionIndex("t1d"), 1)

pn2.addArc("in", pn2.getPlaceIndex("p2a"), pn2.getTransitionIndex("t2d"), 1)
pn2.addArc("out", pn2.getPlaceIndex("p2d"), pn2.getTransitionIndex("t2d"), 1)

pn2.addArc("in", pn2.getPlaceIndex("p0"), pn2.getTransitionIndex("t0leak"), 1)

pn2.setMarking([100, 50, 0, 80, 0])
print(pn2.transition_dict)
rates = [1 for _ in range(len(pn2.allTransitionIndices()))]
rates[1] = 1
#pnp = process.PetriProcessSimpleStochasticAlphaDebiasedRated(pn2, 0.5, rates)
pnp = process.PetriProcessSimpleCTPNRated(pn2, rates)
pnt = tracker.PetriTracker(pn2, record_on_init=True)

def process_callback(to_fire, new_marking, count):
    #print("Count", count)
    pnt.record()
    return False # stop?

pnp.fireContinuously(callback=process_callback, max_cycles=3000, calc_mode='tmatrix')
print(pn2.computeTransitionMatrix())
#print(pnt.time_vector, pnt.markings)
pnt.makeRecordsPlot(pn2.allPlaceIndices(), show=True, place_names=pn2.allPlaceLabels(), differentiation_degree=None, plot_points=True, scatter_points=False, stride=10)
