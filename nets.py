"""
Petri nets.
@author Ruben Walen
"""

import numpy as np
import copy

from . import netreader

class PetriNet:
    """Superclass for Petri nets, with a few useful functions."""
    def __init__(self):
        self.places = [] # place labels
        self.transitions = [] # transition labels
        self.place_dict = {} # dictionary of places (indices) to/from transitions
        self.transition_dict = {} # dictionary of transitions (indices) to/from places
        self.marking = None
        self.transition_matrix = None
        self.time = 0 # how many updates

    def getPlaceLabel(self, index):
        return self.places[index]

    def getPlaceLabels(self, indices):
        return [self.places[i] for i in indices]

    def getTransitionLabel(self, index):
        return self.transitions[index]

    def getTransitionLabels(self, indices):
        return [self.transitions[i] for i in indices]

    def getPlaceIndex(self, label):
        return self.places.index(label)

    def getPlaceIndices(self, labels):
        return [self.places.index(l) for l in labels]

    def getTransitionIndex(self, label):
        return self.transitions.index(label)

    def getTransitionIndices(self, labels):
        return [self.transitions.index(l) for l in labels]

    def getNumPlaces(self):
        return len(self.places)

    def getNumTransitions(self):
        return len(self.transitions)

    def getMarking(self):
        return self.marking

    def setMarking(self, marking):
        self.marking = marking
        return

    def getTime(self):
        return self.time

    def setTime(self, time):
        self.time = time
        return

    def allPlaceIndices(self):
        return [i for i in range(len(self.places))]

    def allTransitionIndices(self):
        return [i for i in range(len(self.transitions))]

    def allPlaceLabels(self):
        return self.places

    def allTransitionLabels(self):
        return self.transitions

class SimplePetriNet(PetriNet):
    """A simple discrete Petri net, non-colored."""
    def addPlace(self, label):
        self.places.append(label)
        index = self.getNumPlaces() - 1
        self.place_dict[index] = {}
        self.place_dict[index]["in"], self.place_dict[index]["out"] = [], []
        return
    
    def addTransition(self, label):
        self.transitions.append(label)
        index = self.getNumTransitions() - 1
        self.transition_dict[index] = {}
        self.transition_dict[index]["in"], self.transition_dict[index]["out"] = [], []
        return

    def addArc(self, direction, place_index, transition_index, weight, return_if_duplicate=False):
        if direction not in ["in", "out"]:
            raise ValueError("@SimplePetriNet.addArc: Invalid direction argument:" + str(direction))
        if weight <= 0:
            raise ValueError("@SimplePetriNet.addArc: Invalid arc weight, arc weights must be positive:" + str(weight))
        
        if (transition_index in [k[0] for k in self.place_dict[place_index][["in", "out"][direction == "in"]]]): # ATTN: only checks place_dict right now
            if not return_if_duplicate:
                raise RuntimeError("@SimplePetriNet.addArc: Arc already exists! " +
                                   str(self.getPlaceLabel(place_index)) + " " + str(self.getTransitionLabel(transition_index)))
            else: # quit if this keyword is used (ignore duplicates)
                return

        self.place_dict[place_index][["in", "out"][direction == "in"]].append((transition_index, weight))
        self.transition_dict[transition_index][direction].append((place_index, weight))
        return

    def setMarking(self, marking):
        marking = np.array(marking)
        if marking.shape[0] != len(self.places):
            raise ValueError("@SimplePetriNet.setMarking: Marking length must equal number of places")
        if marking.ndim != 1:
            raise ValueError("@SimplePetriNet.setMarking: Marking must be a one-dimensional vector")

        self.marking = marking
        return
    
    def computeTransitionMatrix(self, set_to_self=True):
        """Compute the transition matrix"""
        n_transitions = len(self.transitions)
        n_places = len(self.places)
        trans_matrix = np.zeros((n_places, n_transitions))
        for tr in self.transition_dict.keys():
            for entry in self.transition_dict[tr]["in"]:
                # place = entry[0] tokens = entry[1]
                trans_matrix[entry[0], tr] -= entry[1]
            for entry in self.transition_dict[tr]["out"]:
                # place = entry[0] tokens = entry[1]
                trans_matrix[entry[0], tr] += entry[1]

        if set_to_self:
            self.transition_matrix = trans_matrix
        return trans_matrix

    def fireTransitions(self, transitions, skip_liveness_check=False, on_check_fail="stop", calc_mode="tmatrix", update_marking=True,
                        update_time=True):
        """Fire a (series of) transitions simultaneously.
        Args:
            transitions (list): list of transitions to fire simultaneously.
            skip_liveness_check (bool): whether to skip checking whether transition can actually be fired.
                If False, the transitions are fired no matter what.
            on_check_fail ("stop" OR "skip"): what to do when a transition liveness check fails. "stop" to skip calculation,
                "skip" to skip just the calculation of that transition.
            calc_mode ("tmatrix" OR "iterative"): calculation mode for the firing.
            update_marking (bool): whether to update the marking after calculation.
            update_time (bool): whether to update the net time.
        """
        if type(self.marking) == type(None):
            raise RuntimeError("@SimplePetriNet.fireTransitions: no marking set for Petri net!")
        if calc_mode not in ["tmatrix", "transition_matrix", "iterative"]:
            raise ValueError("@SimplePetriNet.fireTransitions: invalid calc_mode: " + str(calc_mode))
        if on_check_fail not in ["stop", "skip"]:
            raise ValueError("@SimplePetriNet.fireTransitions: invalid on_check_fail mode: " + str(on_check_fail))
        if calc_mode == "transition_matrix":
            calc_mode = "tmatrix" # alias resolution
        if calc_mode == "tmatrix":
            if type(self.transition_matrix) == type(None):
                self.computeTransitionMatrix() # calculate the transition matrix if it is not there yet
        if len(transitions) == 0: # do nothing
            if update_time:
                self.time += 1 # update time by one step
            return self.marking

        live_transitions = [True for _ in range(len(transitions))] # default to True
        if not skip_liveness_check:
            live_transitions = self.checkTransitionLiveness(transitions)[0]
            if (on_check_fail == "stop") and (False in live_transitions):
                return False # stop and return False
            # else continue with the live transitions

        live_transition_indices = np.array([transitions[i] for i in range(len(transitions)) if live_transitions[i] == True])
        if calc_mode == "tmatrix":
            calc_matrix = np.zeros((len(self.places), len(transitions)))
            calc_matrix[:, :] = self.transition_matrix[:, live_transition_indices] # ATTN: does this work with multi-firing?
            calc_vector = np.sum(calc_matrix, axis=1)
            new_marking = self.marking + calc_vector # numpy calculation
        elif calc_mode == "iterative":
            new_marking = copy.copy(self.marking)
            for tr in live_transition_indices:
                for entry in self.transition_dict[tr]["in"]:
                    new_marking[entry[0]] -= entry[1] # subtract required tokens
                for entry in self.transition_dict[tr]["out"]:
                    new_marking[entry[0]] += entry[1] # add resulting tokens
                
        if update_marking:
            self.marking = new_marking
        if update_time:
            self.time += 1 # update time by one step
        return new_marking

    def checkTransitionLiveness(self, transitions):
        """Check which transitions are live."""
        live_transitions = [True for _ in range(len(transitions))] # default to True
        marking_depletion = copy.copy(self.marking)
        token_requesters = [[] for _ in range(len(self.places))] # all transitions that request tokens from a place
        for i, tr in enumerate(transitions):
            require = self.transition_dict[tr]["in"]
            for req in require:
                token_requesters[req[0]].append(tr)
                if self.marking[req[0]] < req[1]: # not enough tokens
                    live_transitions[i] = False
                else: # enough tokens, but deplete
                    marking_depletion[req[0]] -= req[1]
        for pl, depl in enumerate(marking_depletion):
            if depl < 0:
                for tr in token_requesters[pl]: # all these transitions are dead because at least one token source is depleted and we cannot pick
                    live_transitions[tr] = False
        live_transition_indices = np.array([transitions[i] for i in range(len(transitions)) if live_transitions[i] == True])
        return live_transitions, live_transition_indices

    def checkSingleLivenessWithMarking(self, transition, marking):
        """Check liveness of a single transition with provided marking."""
        inputs = self.transition_dict[transition]["in"]
        fireable = True
        for mark in inputs:
            if marking[mark[0]] < mark[1]:
                fireable = False
                break
        return fireable

    def constructFromFile(self, filename):
        """Construct (part of) the net from a given filename. Check netreader.py for formatting."""
        information = netreader.readNetFileSimplePetriNet(filename)
        if information['net_type'] != 'simple':
            raise ValueError("@nets.constructFromFile: net_type must be 'simple' for this Petri net type.")
        # version currently unused
        for pl in information['places']:
            self.addPlace(pl)
        for tr in information['transitions']:
            self.addTransition(tr)
        for ar in information['arcs']:
            direction = ar[0]
            place = self.getPlaceIndex(ar[1])
            transition = self.getTransitionIndex(ar[2])
            weight = int(ar[3])
            self.addArc(direction, place, transition, weight)
        return

    def exportToFile(self, filename):
        """Export the Petri net (in format) to filename."""
        file = open(filename, 'w')
        file.write('{')
        file.write('version=0.1,\n')
        file.write('net_type=simple,\n')
        
        file.write('places=[')
        for i, pl in enumerate(self.places):
            file.write(str(pl))
            if i < len(self.places) - 1:
                file.write(',')
        file.write('],\n')

        file.write('transitions=[')
        for i, tr in enumerate(self.transitions):
            file.write(str(tr))
            if i < len(self.transitions) - 1:
                file.write(',')
        file.write('],\n')

        file.write('arcs=[')
        arcs_formatted = []
        for tr in self.transition_dict.keys():
            tr_dict = self.transition_dict[tr]
            for elem in tr_dict["in"]:
                arcs_formatted.append(("in", self.getPlaceLabel(elem[0]), self.getTransitionLabel(tr), elem[1]))
            for elem in tr_dict["out"]:
                arcs_formatted.append(("out", self.getPlaceLabel(elem[0]), self.getTransitionLabel(tr), elem[1]))
        for i, ar in enumerate(arcs_formatted):
            file.write('(' + ar[0] + ',' + str(ar[1]) + ',' + str(ar[2]) + ',' + str(ar[3]) + ')')
            if i < len(arcs_formatted) - 1:
                file.write(',\n')
        file.write('],\n')

        file.write('}')
        file.close()
        return

    def checkPlaceInTransition(self, place_id, transition_id, direction):
        """Check whether a place is connected to a transition (in a certain direction).
            Returns: check (bool), weight (int)"""
        check = False
        for entry in self.transition_dict[transition_id][direction]:
            if entry[0] == place_id:
                check = True
                break
        if check == False:
            return False, None
        else:
            return True, entry[1]

    def reset(self):
        self.time = 0
        self.marking = None
        return

if __name__ == "__main__":
    pn = SimplePetriNet()
    pn.addPlace("p1")
    pn.addPlace("p2")
    pn.addTransition("t1")
    pn.addArc("in", pn.getPlaceIndex("p1"), pn.getTransitionIndex("t1"), 1)
    pn.addArc("out", pn.getPlaceIndex("p2"), pn.getTransitionIndex("t1"), 1)
    try: # should fail
        pn.fireTransitions([pn.getTransitionIndex("t1")])
    except RuntimeError:
        print("Set a marking first")
    pn.setMarking([2, 0])
    for _ in range(3):
        print("Before", pn.marking)
        success = pn.fireTransitions([pn.getTransitionIndex("t1")], calc_mode="iterative")
        print("After", pn.marking, success)
        print("Live transitions:", pn.checkTransitionLiveness(pn.allTransitionIndices())[1])
    print(pn.computeTransitionMatrix())

    import tracker
    import process
    
    pn2 = SimplePetriNet()
    pn2.addPlace("p1a")
    pn2.addPlace("p1b")
    pn2.addPlace("p2")
    pn2.addPlace("p3")
    pn2.addTransition("t1")
    pn2.addTransition("t2")
    pn2.addTransition("t0inf")
    pn2.addArc("in", pn2.getPlaceIndex("p1a"), pn2.getTransitionIndex("t1"), 1)
    pn2.addArc("in", pn2.getPlaceIndex("p1b"), pn2.getTransitionIndex("t1"), 1)
    pn2.addArc("out", pn2.getPlaceIndex("p2"), pn2.getTransitionIndex("t1"), 1)
    pn2.addArc("in", pn2.getPlaceIndex("p2"), pn2.getTransitionIndex("t2"), 1)
    pn2.addArc("out", pn2.getPlaceIndex("p3"), pn2.getTransitionIndex("t2"), 1)
    pn2.addArc("out", pn2.getPlaceIndex("p1a"), pn2.getTransitionIndex("t0inf"), 1)
    pn2.addArc("out", pn2.getPlaceIndex("p1b"), pn2.getTransitionIndex("t0inf"), 2)
    print(pn2.computeTransitionMatrix())
    
    pn2.setMarking([100, 50, 0, 0])
    pnp = process.PetriProcessSimpleStochastic(pn2, 0.2)
    pnt = tracker.PetriTracker(pn2, record_on_init=True)

    def process_callback(to_fire, new_marking, count):
        print("Count", count)
        pnt.record()
        return False # don't stop

    pnp.fireContinuously(callback=process_callback, max_cycles=100)
    #print(pnt.time_vector, pnt.markings)
    pnt.makeRecordsPlot(pn2.allPlaceIndices(), show=True, place_names=pn2.allPlaceLabels(), differentiation_degree=None)

    #pn3 = SimplePetriNet()
    #pn3.constructFromFile("./samplenet.txt")
    #pn3.exportToFile("./samplenet_copy.txt")
    #pn4 = SimplePetriNet()
    #pn4.constructFromFile("./samplenet.txt")
